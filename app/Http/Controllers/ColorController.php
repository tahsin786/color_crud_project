<?php

namespace App\Http\Controllers;
use App\Models\Color;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ColorController extends Controller
{
    public function index()
    {
        $colors = Color::latest()->get();
        // dd($categories);
        return view('backend.colors.index', [
            'colors' => $colors
        ]);
    }

    public function create()
    {
        return view('backend.colors.create');
    }

    public function store(ColorRequest $request)
    {

        try {
            // $request->validate([
            //     'title' => 'required|min:3|max:50|unique:categories,title',
            //     // 'title' => ['required', 'min:3', Rule::unique('categories', 'title')],
            //     'description' => ['required', 'min:10'],
            // ]);
            // $imageName = request()->file('image')->store('storage/app/public/images');
            // dd($imageName);
            Color::create([
                'title' => $request->title,
                
            ]);

            // $request->session()->flash('message', 'Task was successful!');
            return redirect()->route('colors.index')->withMessage('Successfully Created!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
            // dd($e->getMessage());
        }
    }

    // public function show(Category $id)
    public function show(Color $color)
    {
        // $category = Category::where('id', $id)->firstOrFail();
        // $category = Category::find($id);
        return view('backend.colors.show', [
            'color' => $color
        ]);
    }

    public function edit(Color $color)
    {
        return view('backend.colors.edit', [
            'color' => $color
        ]);
    }

    public function update(ColorRequest $request, Color $color)
    {
        try {
            // $request->validate([
            //     // 'title' => 'required|min:3|max:50|unique:categories,title,'.$category->id,
            //     'title' => ['required', 'min:3', 
            //         Rule::unique('categories', 'title')->ignore($category->id)
            //     ],
            //     'description' => ['required', 'min:10'],
            // ]);

            $color->update([
                'title' => $request->title,
                'description' => $request->description,
            ]);

            // $request->session()->flash('message', 'Task was successful!');
            return redirect()->route('colors.index')->withMessage('Successfully Updated!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
            // dd($e->getMessage());
        }
    }

    public function destroy(Color $color)
    {
        try {
            $color->delete();
            return redirect()->route('colors.index')->withMessage('Successfully Deleted!');
        } catch (QueryException $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
            // dd($e->getMessage());
        }
    }
}
